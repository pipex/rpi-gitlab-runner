[![pipeline status](https://gitlab.com/nicosingh/rpi-gitlab-runner/badges/master/pipeline.svg)](https://gitlab.com/nicosingh/rpi-gitlab-runner/commits/master) [![Docker Pulls](https://img.shields.io/docker/pulls/nicosingh/rpi-gitlab-runner.svg)](https://hub.docker.com/r/nicosingh/rpi-gitlab-runner/)

# About

This is a raspbian-based image including a manual installation of gitlab-runner for armhf. Installation process is documented [in here](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/linux-manually.md). Since this installation requires to install docker service, this image is based in [rpi-dind](https://hub.docker.com/r/nicosingh/rpi-dind/) (rpi docker in docker) image.

# How to run this image?

`$ docker run -it --rm nicosingh/rpi-gitlab-runner bash`

Where:

`-it` (optional): if we want to execute an interactive container. Use `-d` if we want to run this image in detached mode.

`--rm` (optional): if we want to remove the container once it finishes.

`nicosingh/rpi-gitlab-runner`: image name.

`bash` (optional): the command we want to execute. For example, open a Bash terminal.

# Environment variables

In order to facilitate gitlab-runner registration process, this image exposes some environment variables

`RUNNER_NAME (e.g. "rpi-runner")`: the name of our gitlab runner container. This name will be displayed at gitlab CI runner configuration.

`CI_SERVER_URL (e.g. "https://gitlab.com/")`: the name of our gitlab repository URL.

`REGISTRATION_TOKEN (e.g. "xxyyzz")`: the token given by our gitlab repository URL.

`DOCKER_PRIVILEGED (e.g. "true")`: required if we are running dind (docker-in-docker) commands in our pipeline.

`DOCKER_VOLUMES (e.g. "/var/run/docker.sock:/var/run/docker.sock")`: required if we are running dind (docker-in-docker) commands in our pipeline.

`RUNNER_EXECUTOR (e.g. "docker")`: name of our gitlab runner executor.

`DOCKER_IMAGE (e.g. "resin/rpi-raspbian")`: name of our docker image to be pulled in our runner executor.

`REGISTER_NON_INTERACTIVE (e.g. "true")`: required to automatically register our gitlab runner.

`RUNNER_TAG_LIST (e.g. "rpi,raspberry,pi")`: comma-separated list of tags that our gitlab runner will have.

If we have these variables defined in our environment before running our container (e.g. `docker-compose` file), it will automatically register our host as a gitlab runner in a non-interactive mode.
